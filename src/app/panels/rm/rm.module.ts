import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TURmComponent } from './rm.component';
import { TURmRoutingModule } from './rm.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        TURmComponent,
    ],
    imports: [
        TURmRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [TURmComponent]
})
export class TURmModule { }
