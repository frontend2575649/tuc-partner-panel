import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon } from '../../../service/service';

@Component({
    selector: 'tu-posterminal',
    templateUrl: './tuposterminal.component.html',
})
export class TUPosTerminalComponent implements OnInit {
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
    ) {

    }
    ngOnInit() {
        this.Form_UpdateUser_Load();
        this._HelperService.AppConfig.ShowHeader = false;
        this._HelperService.ContainerHeight = window.innerHeight;
    }


    public isReadOnly = true;
    EditPassword() {
        if (this.isReadOnly == true) {
            this.isReadOnly = false;
        }
        else {
            this.isReadOnly = true;
        }
    }
    Form_UpdateUser: FormGroup;
    @ViewChild('places') places: GooglePlaceDirective;
    Form_UpdateUser_PlaceMarkerClick(event) {
        this._HelperService._UserAccount.Latitude = event.coords.lat;
        this._HelperService._UserAccount.Longitude = event.coords.lng;
    }
    public Form_UpdateUser_AddressChange(address: Address) {
        this._HelperService._UserAccount.Latitude = address.geometry.location.lat();
        this._HelperService._UserAccount.Longitude = address.geometry.location.lng();
        this._HelperService._UserAccount.Address = address.formatted_address;
    }
    Form_UpdateUser_Show() {
    }
    Form_UpdateUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.CoreUsers]);
    }
    Form_UpdateUser_Load() {
        this._HelperService._FileSelect_Icon_Data.Width = 128;
        this._HelperService._FileSelect_Icon_Data.Height = 128;

        this._HelperService._FileSelect_Poster_Data.Width = 800;
        this._HelperService._FileSelect_Poster_Data.Height = 400;

        this.Form_UpdateUser = this._FormBuilder.group({
            OperationType: 'new',
            Task: this._HelperService.AppConfig.Api.Core.UpdateUserAccount,
            UserName: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(256)])],
            StatusCode: this._HelperService.AppConfig.Status.Inactive,
        });
    }
    Form_UpdateUser_Clear() {
        this._HelperService._FileSelect_Icon_Reset();
        this._HelperService._FileSelect_Poster_Reset();
    }
    Form_UpdateUser_Process(_FormValue: any) {
        swal({
            position: 'top',
            title: this._HelperService.AppConfig.CommonResource.UpdateTitle,
            text: this._HelperService.AppConfig.CommonResource.UpdateHelp,
            input: 'password',
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
            inputAttributes: {
                autocapitalize: 'off',
                autocorrect: 'off',
                maxLength: "4",
                minLength: "4"
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                _FormValue.DisplayName = _FormValue.UserName;
                _FormValue.AuthPin = result.value;
                _FormValue.ReferenceKey = this._HelperService.AppConfig.ActiveReferenceKey;
                this._HelperService.IsFormProcessing = true;
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, _FormValue);
                _OResponse.subscribe(
                    _Response => {
                        this._HelperService.IsFormProcessing = false;
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this.Form_UpdateUser_Clear();
                            if (_FormValue.OperationType == 'close') {
                                this.Form_UpdateUser_Close();
                            }
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                    });
            }
        });

    }

}