import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { Select2Module } from "ng2-select2";
import { TranslateModule } from "@ngx-translate/core";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";

import { HCListComponent } from "./hclist.component";
const routes: Routes = [{ path: "", component: HCListComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HCListRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        HCListRoutingModule,
        NgxPaginationModule,
        Daterangepicker,
        Select2Module
    ],
    declarations: [HCListComponent]
})
export class HCListModule { }
