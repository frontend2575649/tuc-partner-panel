import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Observable, of } from "rxjs";
import { ActivatedRoute, Router, Params } from "@angular/router";
import {
  OSelect,
  OList,
  DataHelperService,
  HelperService,
  OResponse,
  OStorageContent,
  OCoreParameter,
  OCoreCommon
} from "../../../../service/service";
import swal from "sweetalert2";

import * as Feather from 'feather-icons';

@Component({
  selector: "tu-stores",
  templateUrl: "./tustores.component.html"
})
export class TUStoresComponent implements OnInit {
  Type: number
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
    this._HelperService.ShowDateRange = false;
  }
  ngOnInit() {
    Feather.replace();
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this.Type = params["type"]
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null || this._HelperService.AppConfig.ActiveReferenceId == null) {
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
      } else {
        // this._HelperService.Get_UserAccountDetails(true);
        this.StoresList_Filter_Owners_Load();
        this.StoresList_Setup();
      }
    });
  }

  public StoresList_Config: OList;
  StoresList_Setup() {
    var SearchCondition = undefined;
    if (this._HelperService.AppConfig.ActiveReferenceId != 0 && this._HelperService.AppConfig.ActiveReferenceKey != undefined && this._HelperService.AppConfig.ActiveReferenceKey != null && this._HelperService.AppConfig.ActiveReferenceKey != "") {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveReferenceId, '=');
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerKey', this._HelperService.AppConfig.DataType.Text, this._HelperService.AppConfig.ActiveReferenceKey, '=');
    } else {
      SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'OwnerId', this._HelperService.AppConfig.DataType.Number, "0", '>');
    }
    if (SearchCondition != undefined) {
      this.StoresList_Config = {
        Id: "StoresList",
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        SubReferenceId: this._HelperService.UserAccount.AccountId,
        SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Title: "All Stores",
        StatusType: "default",
        DefaultSortExpression: 'CreateDate desc',
        // SearchBaseCondition: SearchCondition,
        TableFields: [
          {
            DisplayName: 'Name',
            SystemName: 'DisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'ContactNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Email Address',
            SystemName: 'EmailAddress',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Terminals',
            SystemName: 'Terminals',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'R. %',
            SystemName: 'RewardPercentage',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '_600',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Tr',
            SystemName: 'LastTransactionDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Login',
            SystemName: 'LastLoginDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
        ]
      };
      if (this.Type == 6) {
        this.StoresList_Config.Type = this._HelperService.AppConfig.ListType.Manager

      }
      else {
        this.StoresList_Config.Type = this._HelperService.AppConfig.ListType.RM

      }
      // this.StoresList_Config.Type = 

    }
    else {
      this.StoresList_Config = {
        Id: null,
        Sort: null,
        Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
        Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
        SubReferenceId: this._HelperService.UserAccount.AccountId,
        SubReferenceKey: this._HelperService.UserAccount.AccountKey,
        ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
        ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
        Title: "All Stores",
        StatusType: "default",
        Type: this._HelperService.AppConfig.ListType.All,
        DefaultSortExpression: 'CreateDate desc',
        TableFields: [
          {
            DisplayName: 'Name',
            SystemName: 'DisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: true,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Contact No',
            SystemName: 'ContactNumber',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Email Address',
            SystemName: 'EmailAddress',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Merchant',
            SystemName: 'OwnerDisplayName',
            DataType: this._HelperService.AppConfig.DataType.Text,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Terminals',
            SystemName: 'Terminals',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'R. %',
            SystemName: 'RewardPercentage',
            DataType: this._HelperService.AppConfig.DataType.Number,
            Class: '_600',
            Show: true,
            Search: false,
            Sort: false,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Tr',
            SystemName: 'LastTransactionDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
          {
            DisplayName: 'Last Login',
            SystemName: 'LastLoginDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: false,
            Search: false,
            Sort: false,
            ResourceId: null,
          },

          {
            DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
            SystemName: 'CreateDate',
            DataType: this._HelperService.AppConfig.DataType.Date,
            Class: 'td-date',
            Show: true,
            Search: false,
            Sort: true,
            ResourceId: null,
          },
        ]

      };
    }

    this.StoresList_Config = this._DataHelperService.List_Initialize(
      this.StoresList_Config
    );
    this.StoresList_GetData();
  }
  StoresList_ToggleOption(event: any, Type: any) {

    if (event != null) {
    for (let index = 0; index < this.StoresList_Config.Sort.SortOptions.length; index++) {
      const element = this.StoresList_Config.Sort.SortOptions[index];
      if (event.SystemName == element.SystemName) {

          element.SystemActive = true;
        
      }
      else {
          element.SystemActive = false;

      }
    }
  }

    this.StoresList_Config = this._DataHelperService.List_Operations(
      this.StoresList_Config,
      event,
      Type
    );
    if (this.StoresList_Config.RefreshData == true) {
      this.StoresList_GetData();
    }
    this._HelperService.StopClickPropogation();
  }
  StoresList_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.StoresList_Config
    );
    this.StoresList_Config = TConfig;
  }
  StoresList_RowSelected(ReferenceData) {
    // this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store.Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId]);
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveStore,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer.Store
        .Dashboard,
      ReferenceData.ReferenceKey,
      ReferenceData.ReferenceId,
    ]);
 

  }

  public StoresList_Filter_Owners_Option: Select2Options;
  public StoresList_Filter_Owners_Selected = 0;
  StoresList_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
      ]
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.StoresList_Filter_Owners_Option = {
      placeholder: 'Sort by Owner',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  StoresList_Filter_Owners_Change(event: any) {
    if (event.value == this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = 0;
    }
    else if (event.value != this.StoresList_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '=');
      this.StoresList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.StoresList_Config.SearchBaseConditions);
      this.StoresList_Filter_Owners_Selected = event.value;
      this.StoresList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this.StoresList_Filter_Owners_Selected, '='));
    }

    this.StoresList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  public ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    
    this.StoresList_Filter_Owners_Load();
    this.StoresList_Setup();
    
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();

  }

}
