import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
declare var moment: any;
import swal from "sweetalert2";
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent, OCoreParameter, OCoreCommon, OOverview } from '../../../../service/service';
import { DaterangePickerComponent } from 'ng2-daterangepicker';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { ChangeContext } from 'ng5-slider';

@Component({
    selector: "turmreport",
    templateUrl: "./turmreport.component.html",
    styles: [
        `
      agm-map {
        height: 300px;
      }
    `
    ]
})
export class TURmReportComponent implements OnInit {
   
  

  public Options: any = {
    cornerRadius: 20,
    responsive: true,
    elements: {
      line: {
        tension: 0
      }
    },
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    // annotation: {
    //   annotations: [{
    //     type: 'line',
    //     mode: 'horizontal',
    //     scaleID: 'y-axis-0',
    //     value: 20,
    //     borderColor: 'rgb(75, 192, 192)',
    //     borderWidth: 4,
    //     label: {
    //       enabled: false,
    //       content: 'Test label'
    //     }
    //   }]
    // },
    plugins: {
      datalabels: {
        display: false,
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      }
    }
  }
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {

  }


  public barChartLabels = ['9 AM ', '10 AM', '11 AM', '12 PM', '01 PM', "02 PM", "03 PM"];
  // public barChartColors = [{ backgroundColor: ['#00D1D4', '#00D1D4', '#00D1D4', '#00D1D4', '#00D1D4', '#00D1D4', '#00D1D4'] }, { backgroundColor: ['#FFD000', '#FFD000', '#FFD000', '#FFD000', '#FFD000', '#FFD000', '#FFD000'] }, { backgroundColor: ['#E5000C', '#E5000C', '#E5000C', '#E5000C', , '#E5000C', '#E5000C', '#E5000C'] }, { backgroundColor: ['#3C00FF', '#3C00FF', '#3C00FF', '#3C00FF', '#3C00FF', '#3C00FF', '#3C00FF'] }];
  public barChartType = 'line';
  public barChartLegend = true;
  public barChartData = [
    { data: [63, 55, 80, 62, 51, 62, 51], label: 'Remote', fill: false, backgroundColor: ["#bae755"] },
    { data: [28, 48, 40, 81, 20, 63, 77], label: 'Visit', fill: false },
  ];
  public TodayStartTime = null;
  public TodayEndTime = null;


  Type = 5;
  StartTime = null;
  StartTimeS = null;
  EndTime = null;
  EndTimeS = null;
  CustomType = 1;


  public CampaignKey = null;
  CampaignRewardType_FixedAmount = "fixedamount";
  CampaignRewardType_AmountPercentage = "amountpercentage";
  @ViewChild(DaterangePickerComponent)
  private _DaterangePickerComponent: DaterangePickerComponent;


  ngOnInit() {
  
    this.GetBranches_List();
    this.GetMangers_List()

  }

  



  public GetBranches_Option: Select2Options;
  public GetBranches_Transport: any;
  GetBranches_List() {
      var PlaceHolder = "Select Branch";
      var _Select: OSelect =
      {
          Task: this._HelperService.AppConfig.Api.Core.GetBranches,
          Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
          ReferenceKey: this._HelperService.UserAccount.AccountKey,
          ReferenceId: this._HelperService.UserAccount.AccountId,
          SearchCondition: "",
          SortCondition: [],
          Fields: [
              {
                  SystemName: "ReferenceId",
                  Type: this._HelperService.AppConfig.DataType.Number,
                  Id: true,
                  Text: false,
              },
              {
                  SystemName: "DisplayName",
                  Type: this._HelperService.AppConfig.DataType.Text,
                  Id: false,
                  Text: true
              },
              // {
              //     SystemName: "AccountTypeCode",
              //     Type: this._HelperService.AppConfig.DataType.Text,
              //     SearchCondition: "=",
              //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
              // }
          ]
      }
      this.GetBranches_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
      this.GetBranches_Option = {
          placeholder: PlaceHolder,
          ajax: this.GetBranches_Transport,
          multiple: false,
      };
  }
  GetBranches_ListChange(event: any) {
      // alert(event);
      // this.Form_AddUser.patchValue(
      //     {
      //         RoleKey: event.value
      //     }
      // );
  }

  public GetMangers_Option: Select2Options;
  public GetMangers_Transport: any;
  GetMangers_List() {
      var PlaceHolder = "Select Manager";
      var _Select: OSelect =
      {
          Task: this._HelperService.AppConfig.Api.Core.GetManagers,
          Location: this._HelperService.AppConfig.NetworkLocation.V3.Branch,
          ReferenceKey: this._HelperService.UserAccount.AccountKey,
          ReferenceId: this._HelperService.UserAccount.AccountId,
          SearchCondition: "",
          SortCondition: [],
          Fields: [
              {
                  SystemName: "ReferenceId",
                  Type: this._HelperService.AppConfig.DataType.Number,
                  Id: true,
                  Text: false,
              },
              {
                  SystemName: "Name",
                  Type: this._HelperService.AppConfig.DataType.Text,
                  Id: false,
                  Text: true
              },
              // {
              //     SystemName: "AccountTypeCode",
              //     Type: this._HelperService.AppConfig.DataType.Text,
              //     SearchCondition: "=",
              //     SearchValue: this._HelperService.AppConfig.AccountType.RelationshipManager
              // }
          ]
      }

      _Select.SearchCondition = this._HelperService.GetSearchConditionStrict(_Select.SearchCondition, 'RoleId', this._HelperService.AppConfig.DataType.Text, '8', '=');
      this.GetMangers_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
      this.GetMangers_Option = {
          placeholder: "All RMs",
          ajax: this.GetMangers_Transport,
          multiple: false,
      };
  }
  GetMangers_ListChange(event: any) {

  }







}